extends Area2D


const Explosion = preload("res://explosions/bullet_explosion.tscn")


var owner_ship: Node2D = null
var velocity := Vector2.ZERO
var lifetime: float


func _ready() -> void:
	$lifetime.start(lifetime)
	
	if owner_ship.red_ship:
		$blue_bullet.hide()
		$red_bullet.show()
		collision_mask = 1

func _physics_process(dt: float) -> void:
	position += velocity * dt
	position = Globals.wrap(position)


func _on_lifetime_timeout() -> void:
	var inst = Explosion.instance()
	inst.position = position
	get_parent().add_child(inst)
	queue_free()


func _on_bullet_area_entered(area: Area2D) -> void:
	if area.dead:
		return
		
	area.take_damages(1)
	_on_lifetime_timeout()
