extends Node2D


var p0_score = 0
var p1_score = 0

var ship_collided_ship = false


func _ready() -> void:
	round_start()


func _draw() -> void:
	draw_rect(Rect2(0, 0, 1280, 720), Color.black)
	
	for i in range(100):
		draw_rect(
			Rect2(rand_range(0, 1280), rand_range(0, 720), 2, 2),
			Color(rand_range(0.5, 1), 1, 1)
		)



func _input(event: InputEvent) -> void:
	if event.is_action("p0_accelerate") or event.is_action("p1_accelerate"):
		$gui/instructions.hide()



func round_start() -> void:
	ship_collided_ship = false
	get_tree().call_group("bullet", "queue_free")
	
	$blue_ship.reset()
	$red_ship.reset()
	
	$gui/instructions.show()

func end_round(red: bool) -> void:
	if not ship_collided_ship:
		if red:
			p0_score += 1
		else:
			p1_score += 1
	
		$gui/p0_score.text = String(p0_score)
		$gui/p1_score.text = String(p1_score)
	
	$anim.play("end_round")


func _on_ship_collided_ship() -> void:
	ship_collided_ship = true
