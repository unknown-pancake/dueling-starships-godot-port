class_name Globals extends Node



static func wrap(p: Vector2) -> Vector2:
	if p.x < 0: p.x += 1280
	if p.x > 1280: p.x -= 1280
	if p.y < 720: p.y += 720
	if p.y > 720: p.y -= 720
	return p
