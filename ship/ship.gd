extends Area2D


signal died(red)
signal collided_ship()


const Explosion = preload("res://explosions/ship_explosion.tscn")
const Bullet = preload("res://bullet/bullet.tscn")


export var red_ship := false

var velocity := Vector2.ZERO
var damages := 0
var dead := false


func _ready() -> void:
	if red_ship:
		$ship_blue.hide()
		$ship_red.show()
		collision_layer = 2
		collision_mask = 1
	
	update_health()
	
func _physics_process(dt: float) -> void:
	if dead:
		return
	
	dispatch_accelerate_ship(dt)
	dispatch_turn_ship(dt)
	move_ship(dt)
	shoot()


func _input(event: InputEvent) -> void:
	mine(event)
	


func dispatch_accelerate_ship(dt: float) -> void:
	if red_ship:
		accelerate_ship(dt, Input.is_action_pressed("p1_accelerate"))
	else:
		accelerate_ship(dt, Input.is_action_pressed("p0_accelerate"))

func accelerate_ship(dt: float, should_move_ship: bool) -> void:
	should_move_ship = should_move_ship and abs(velocity.x + velocity.y) < (5*60)
	
	if should_move_ship:
		$thrust.emitting = true
		velocity += Vector2.RIGHT.rotated(rotation) * 0.050*60
	else:
		$thrust.emitting = false
		velocity *= 0.99



func dispatch_turn_ship(dt: float) -> void:
	if red_ship:
		turn_ship(dt, Input.get_action_strength("p1_turn_left") - Input.get_action_strength("p1_turn_right") )
	else:
		turn_ship(dt, Input.get_action_strength("p0_turn_left") - Input.get_action_strength("p0_turn_right") )

func turn_ship(dt: float, dir: float) -> void:
	rotation_degrees -= (3*60*dt) * dir
	$health.rotation = -rotation


func move_ship(dt: float) -> void:
	position += velocity * dt
	position = Globals.wrap(position)


func shoot() -> void:
	var shoot_bullet = false
	
	if red_ship:
		shoot_bullet = Input.is_action_pressed("p1_fire")
	else:
		shoot_bullet = Input.is_action_pressed("p0_fire")
		
	if shoot_bullet and $shoot_timer.is_stopped():
		var bullet = Bullet.instance()
		bullet.owner_ship = self
		bullet.rotation = rotation 
		bullet.velocity = velocity + Vector2.RIGHT.rotated(rotation) * 5*60
		bullet.position = position + Vector2.RIGHT.rotated(rotation) * 32
		bullet.modulate.a = modulate.a
		bullet.lifetime = 2
		get_parent().add_child(bullet)
		$shoot_timer.start()

func mine(ev: InputEvent) -> void:
	var shoot_mine = false
	
	if red_ship:
		shoot_mine = ev.is_action_pressed("p1_mine")
	else:
		shoot_mine = ev.is_action_pressed("p0_mine")
	
	if not shoot_mine:
		return
	
	var bullet = Bullet.instance()
	bullet.owner_ship = self 
	bullet.rotation = rand_range(-PI, PI)
	bullet.velocity = Vector2.RIGHT.rotated(bullet.rotation) * 0.02*60
	bullet.position = position - Vector2.RIGHT.rotated(rotation) * 50
	bullet.modulate.a = modulate.a
	bullet.lifetime = 10
	get_parent().add_child(bullet)


func take_damages(amnt := 1) -> void:
	if dead:
		return
	
	damages = min(5, damages + amnt)
	
	update_health()
	if damages == 5:
		dead = true
		hide()
		var ex = Explosion.instance()
		ex.position = position
		get_parent().add_child(ex)
		emit_signal("died", red_ship)

func update_health() -> void:
	var health = max(0, 5 - damages)
	
	var i = 5
	while i > 0:
		i -= 1
		
		if red_ship:
			$health/vb.get_child(i).texture = preload("res://sprites/red_bullet.png")
		
		$health/vb.get_child(i).visible = health > i



func reset() -> void:
	if red_ship:
		position = Vector2(
			1280/2+400, 
			720/2+rand_range(-250, 250)
		)
	else:
		position = Vector2(
			1280/2-400, 
			720/2+rand_range(-250, 250)
		)
	
	damages = 0
	velocity = Vector2.ZERO
	rotation = PI if red_ship else 0
	dead = false
	show()
	
	update_health()


func _on_ship_area_entered(area: Area2D) -> void:
	if area.is_in_group("ship") and not area.dead and not dead:
		emit_signal("collided_ship")
		take_damages(5)
		area.take_damages(5)
