[gd_scene load_steps=11 format=2]

[ext_resource path="res://sprites/ship_red.png" type="Texture" id=1]
[ext_resource path="res://sprites/ship_blue.png" type="Texture" id=2]
[ext_resource path="res://ship/ship.gd" type="Script" id=3]
[ext_resource path="res://sprites/flame.png" type="Texture" id=4]
[ext_resource path="res://sprites/blue_bullet.png" type="Texture" id=5]

[sub_resource type="Shader" id=1]
code = "shader_type particles;
uniform vec3 direction;
uniform float spread;
uniform float flatness;
uniform float initial_linear_velocity;
uniform float initial_angle;
uniform float angular_velocity;
uniform float orbit_velocity;
uniform float linear_accel;
uniform float radial_accel;
uniform float tangent_accel;
uniform float damping;
uniform float scale;
uniform float hue_variation;
uniform float anim_speed;
uniform float anim_offset;
uniform float initial_linear_velocity_random;
uniform float initial_angle_random;
uniform float angular_velocity_random;
uniform float orbit_velocity_random;
uniform float linear_accel_random;
uniform float radial_accel_random;
uniform float tangent_accel_random;
uniform float damping_random;
uniform float scale_random;
uniform float hue_variation_random;
uniform float anim_speed_random;
uniform float anim_offset_random;
uniform float lifetime_randomness;
uniform vec3 emission_box_extents;
uniform vec4 color_value : hint_color;
uniform int trail_divisor;
uniform vec3 gravity;
uniform sampler2D color_ramp;


float rand_from_seed(inout uint seed) {
	int k;
	int s = int(seed);
	if (s == 0)
	s = 305420679;
	k = s / 127773;
	s = 16807 * (s - k * 127773) - 2836 * k;
	if (s < 0)
		s += 2147483647;
	seed = uint(s);
	return float(seed % uint(65536)) / 65535.0;
}

float rand_from_seed_m1_p1(inout uint seed) {
	return rand_from_seed(seed) * 2.0 - 1.0;
}

uint hash(uint x) {
	x = ((x >> uint(16)) ^ x) * uint(73244475);
	x = ((x >> uint(16)) ^ x) * uint(73244475);
	x = (x >> uint(16)) ^ x;
	return x;
}

void vertex() {
	uint base_number = NUMBER / uint(trail_divisor);
	uint alt_seed = hash(base_number + uint(1) + RANDOM_SEED);
	float angle_rand = rand_from_seed(alt_seed);
	float scale_rand = rand_from_seed(alt_seed);
	float hue_rot_rand = rand_from_seed(alt_seed);
	float anim_offset_rand = rand_from_seed(alt_seed);
	float pi = 3.14159;
	float degree_to_rad = pi / 180.0;

	bool restart = false;
	float tv = 0.0;
	if (CUSTOM.y > CUSTOM.w) {
		restart = true;
		tv = 1.0;
	}

	if (RESTART || restart) {
		float tex_linear_velocity = 0.0;
		float tex_angle = 0.0;
		float tex_anim_offset = 0.0;
		float spread_rad = spread * degree_to_rad;
		float angle1_rad = rand_from_seed_m1_p1(alt_seed) * spread_rad;
		angle1_rad += direction.x != 0.0 ? atan(direction.y, direction.x) : sign(direction.y) * (pi / 2.0);
		vec3 rot = vec3(cos(angle1_rad), sin(angle1_rad), 0.0);
		VELOCITY = rot * initial_linear_velocity * mix(1.0, rand_from_seed(alt_seed), initial_linear_velocity_random);
		float base_angle = (initial_angle + tex_angle) * mix(1.0, angle_rand, initial_angle_random);
		CUSTOM.x = base_angle * degree_to_rad;
		CUSTOM.y = 0.0;
		CUSTOM.w = (1.0 - lifetime_randomness * rand_from_seed(alt_seed));
		CUSTOM.z = (anim_offset + tex_anim_offset) * mix(1.0, anim_offset_rand, anim_offset_random);
		TRANSFORM[3].xyz = vec3(rand_from_seed(alt_seed) * 2.0 - 1.0, rand_from_seed(alt_seed) * 2.0 - 1.0, rand_from_seed(alt_seed) * 2.0 - 1.0) * emission_box_extents;
		VELOCITY = (EMISSION_TRANSFORM * vec4(VELOCITY, 0.0)).xyz;
		TRANSFORM = EMISSION_TRANSFORM * TRANSFORM;
		VELOCITY.z = 0.0;
		TRANSFORM[3].z = 0.0;
	} else {
		CUSTOM.y += DELTA / LIFETIME;
		tv = CUSTOM.y / CUSTOM.w;
		float tex_linear_velocity = 0.0;
		float tex_orbit_velocity = 0.0;
		float tex_angular_velocity = 0.0;
		float tex_linear_accel = 0.0;
		float tex_radial_accel = 0.0;
		float tex_tangent_accel = 0.0;
		float tex_damping = 0.0;
		float tex_angle = 0.0;
		float tex_anim_speed = 0.0;
		float tex_anim_offset = 0.0;
		vec3 force = gravity;
		vec3 pos = TRANSFORM[3].xyz;
		pos.z = 0.0;
		// apply linear acceleration
		force += length(VELOCITY) > 0.0 ? normalize(VELOCITY) * (linear_accel + tex_linear_accel) * mix(1.0, rand_from_seed(alt_seed), linear_accel_random) : vec3(0.0);
		// apply radial acceleration
		vec3 org = EMISSION_TRANSFORM[3].xyz;
		vec3 diff = pos - org;
		force += length(diff) > 0.0 ? normalize(diff) * (radial_accel + tex_radial_accel) * mix(1.0, rand_from_seed(alt_seed), radial_accel_random) : vec3(0.0);
		// apply tangential acceleration;
		force += length(diff.yx) > 0.0 ? vec3(normalize(diff.yx * vec2(-1.0, 1.0)), 0.0) * ((tangent_accel + tex_tangent_accel) * mix(1.0, rand_from_seed(alt_seed), tangent_accel_random)) : vec3(0.0);
		// apply attractor forces
		VELOCITY += force * DELTA;
		// orbit velocity
		float orbit_amount = (orbit_velocity + tex_orbit_velocity) * mix(1.0, rand_from_seed(alt_seed), orbit_velocity_random);
		if (orbit_amount != 0.0) {
		     float ang = orbit_amount * DELTA * pi * 2.0;
		     mat2 rot = mat2(vec2(cos(ang), -sin(ang)), vec2(sin(ang), cos(ang)));
		     TRANSFORM[3].xy -= diff.xy;
		     TRANSFORM[3].xy += rot * diff.xy;
		}
		if (damping + tex_damping > 0.0) {
			float v = length(VELOCITY);
			float damp = (damping + tex_damping) * mix(1.0, rand_from_seed(alt_seed), damping_random);
			v -= damp * DELTA;
			if (v < 0.0) {
				VELOCITY = vec3(0.0);
			} else {
				VELOCITY = normalize(VELOCITY) * v;
			}
		}
		float base_angle = (initial_angle + tex_angle) * mix(1.0, angle_rand, initial_angle_random);
		base_angle += CUSTOM.y * LIFETIME * (angular_velocity + tex_angular_velocity) * mix(1.0, rand_from_seed(alt_seed) * 2.0 - 1.0, angular_velocity_random);
		CUSTOM.x = base_angle * degree_to_rad;
		CUSTOM.z = (anim_offset + tex_anim_offset) * mix(1.0, anim_offset_rand, anim_offset_random) + CUSTOM.y * (anim_speed + tex_anim_speed) * mix(1.0, rand_from_seed(alt_seed), anim_speed_random);
	}
	float tex_scale = 1.0;
	float tex_hue_variation = 0.0;
	float hue_rot_angle = (hue_variation + tex_hue_variation) * pi * 2.0 * mix(1.0, hue_rot_rand * 2.0 - 1.0, hue_variation_random);
	float hue_rot_c = cos(hue_rot_angle);
	float hue_rot_s = sin(hue_rot_angle);
	mat4 hue_rot_mat = mat4(vec4(0.299, 0.587, 0.114, 0.0),
			vec4(0.299, 0.587, 0.114, 0.0),
			vec4(0.299, 0.587, 0.114, 0.0),
			vec4(0.000, 0.000, 0.000, 1.0)) +
		mat4(vec4(0.701, -0.587, -0.114, 0.0),
			vec4(-0.299, 0.413, -0.114, 0.0),
			vec4(-0.300, -0.588, 0.886, 0.0),
			vec4(0.000, 0.000, 0.000, 0.0)) * hue_rot_c +
		mat4(vec4(0.168, 0.330, -0.497, 0.0),
			vec4(-0.328, 0.035,  0.292, 0.0),
			vec4(1.250, -1.050, -0.203, 0.0),
			vec4(0.000, 0.000, 0.000, 0.0)) * hue_rot_s;
	COLOR = hue_rot_mat * textureLod(color_ramp, vec2(tv, 0.0), 0.0);

	TRANSFORM[0] = vec4(cos(CUSTOM.x), -sin(CUSTOM.x), 0.0, 0.0);
	TRANSFORM[1] = vec4(sin(CUSTOM.x), cos(CUSTOM.x), 0.0, 0.0);
	TRANSFORM[2] = vec4(0.0, 0.0, 1.0, 0.0);
	float base_scale = tex_scale * mix(scale, 1.0, scale_random * scale_rand);
	if (base_scale < 0.000001) {
		base_scale = 0.000001;
	}
	TRANSFORM[0].xyz *= base_scale;
	TRANSFORM[1].xyz *= base_scale;
	TRANSFORM[2].xyz *= base_scale;
	VELOCITY.z = 0.0;
	TRANSFORM[3].z = 0.0;
	if (CUSTOM.y > CUSTOM.w) {		ACTIVE = false;
	}
	
	VELOCITY.xyz *= 0.9;
	vec3 pos = TRANSFORM[3].xyz;
	if( pos.x < 0.0 ) { pos.x += 1280.0 }
	if( pos.x > 1280.0 ) { pos.x -= 1280.0 }
	if( pos.y < 0.0 ) { pos.y += 720.0 }
	if( pos.y > 720.0 ) { pos.y -= 720.0 }
	TRANSFORM[3].xyz = pos;
}

"

[sub_resource type="Gradient" id=2]
offsets = PoolRealArray( 0, 0.454545, 1 )
colors = PoolColorArray( 1, 1, 1, 1, 1, 1, 1, 0.498039, 1, 1, 1, 0 )

[sub_resource type="GradientTexture" id=3]
gradient = SubResource( 2 )

[sub_resource type="ShaderMaterial" id=4]
shader = SubResource( 1 )
shader_param/direction = Vector3( -1, 0, 0 )
shader_param/spread = 30.0
shader_param/flatness = 0.0
shader_param/initial_linear_velocity = 100.0
shader_param/initial_angle = 0.0
shader_param/angular_velocity = 0.0
shader_param/orbit_velocity = 0.0
shader_param/linear_accel = 0.0
shader_param/radial_accel = 0.0
shader_param/tangent_accel = 0.0
shader_param/damping = 0.0
shader_param/scale = 2.0
shader_param/hue_variation = 0.0
shader_param/anim_speed = 0.0
shader_param/anim_offset = 0.0
shader_param/initial_linear_velocity_random = 1.0
shader_param/initial_angle_random = 0.0
shader_param/angular_velocity_random = 0.0
shader_param/orbit_velocity_random = 0.0
shader_param/linear_accel_random = 0.0
shader_param/radial_accel_random = 0.0
shader_param/tangent_accel_random = 0.0
shader_param/damping_random = 0.0
shader_param/scale_random = 0.0
shader_param/hue_variation_random = 0.0
shader_param/anim_speed_random = 0.0
shader_param/anim_offset_random = 0.0
shader_param/lifetime_randomness = 0.0
shader_param/emission_box_extents = Vector3( 2.5, 2.5, 0 )
shader_param/color_value = Color( 1, 1, 1, 1 )
shader_param/trail_divisor = 1
shader_param/gravity = Vector3( 0, -1e-06, 0 )
shader_param/color_ramp = SubResource( 3 )

[sub_resource type="RectangleShape2D" id=5]
extents = Vector2( 15.3061, 15.4575 )

[node name="ship" type="Area2D" groups=[
"ship",
]]
scale = Vector2( 2, 2 )
collision_mask = 2
script = ExtResource( 3 )

[node name="ship_blue" type="Sprite" parent="."]
texture = ExtResource( 2 )

[node name="ship_red" type="Sprite" parent="."]
visible = false
texture = ExtResource( 1 )

[node name="thrust" type="Particles2D" parent="."]
position = Vector2( -20, 0 )
emitting = false
amount = 60
lifetime = 0.5
local_coords = false
process_material = SubResource( 4 )
texture = ExtResource( 4 )

[node name="shoot_timer" type="Timer" parent="."]
process_mode = 0
wait_time = 0.2
one_shot = true

[node name="shape" type="CollisionShape2D" parent="."]
shape = SubResource( 5 )

[node name="health" type="Node2D" parent="."]

[node name="vb" type="HBoxContainer" parent="health"]
margin_left = -87.0
margin_top = -26.0
margin_right = 87.0
margin_bottom = -21.0
custom_constants/separation = 10
alignment = 1
__meta__ = {
"_edit_use_anchors_": false
}

[node name="hp0" type="TextureRect" parent="health/vb"]
margin_left = 54.0
margin_right = 59.0
margin_bottom = 5.0
rect_min_size = Vector2( 5, 5 )
texture = ExtResource( 5 )
expand = true

[node name="hp1" type="TextureRect" parent="health/vb"]
margin_left = 69.0
margin_right = 74.0
margin_bottom = 5.0
rect_min_size = Vector2( 5, 5 )
texture = ExtResource( 5 )
expand = true

[node name="hp2" type="TextureRect" parent="health/vb"]
margin_left = 84.0
margin_right = 89.0
margin_bottom = 5.0
rect_min_size = Vector2( 5, 5 )
texture = ExtResource( 5 )
expand = true

[node name="hp3" type="TextureRect" parent="health/vb"]
margin_left = 99.0
margin_right = 104.0
margin_bottom = 5.0
rect_min_size = Vector2( 5, 5 )
texture = ExtResource( 5 )
expand = true

[node name="hp4" type="TextureRect" parent="health/vb"]
margin_left = 114.0
margin_right = 119.0
margin_bottom = 5.0
rect_min_size = Vector2( 5, 5 )
texture = ExtResource( 5 )
expand = true

[connection signal="area_entered" from="." to="." method="_on_ship_area_entered"]
